package com.example.xavin.loginformwithsql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class UserDataDatabase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="UserData.db";
    public static final String TABLE_NAME="User";
    public static final String col_1="ID";
    public static final String col_2="NAME";
    public static final String col_3="AGE";
    public static final String col_4="SEX";
    public static final String col_5="EMAIL";
    public static final String col_6="PHONE";


    public UserDataDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+"("+col_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+col_2+" STRING, "+col_3+" STRING,"+col_4+" STRING,"+col_5+" STRING,"+col_6+" STRING)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }
    public boolean insertData(String name, String age, String sex, String email, String phone)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(col_2,name);
        value.put(col_3,age);
        value.put(col_4,sex);
        value.put(col_5,email);
        value.put(col_6,phone);
        long result = db.insert(TABLE_NAME,null,value);

        if (result==-1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public Cursor getData()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

}

