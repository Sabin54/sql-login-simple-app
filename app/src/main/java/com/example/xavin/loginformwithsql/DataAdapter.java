package com.example.xavin.loginformwithsql;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.BookViewHolder> {
    private List<DataModel> personalData;

    public DataAdapter(List<DataModel> personalData) {
        this.personalData = personalData;
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_data, parent, false);

        return new BookViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        holder.Username.setText(personalData.get(position).getUsername());
        holder.Age.setText(personalData.get(position).getAge());
        holder.Sex.setText(personalData.get(position).getSex());
        holder.Email.setText(personalData.get(position).getEmail());
        holder.Phone.setText(personalData.get(position).getPhone());

    }

    @Override
    public int getItemCount() {
        return personalData.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        public TextView Username;
        public TextView Age;
        public TextView Sex;
        public TextView Email;
        public TextView Phone;

        public BookViewHolder(View view) {
            super(view);
            Username = (TextView) view.findViewById(R.id.name);
            Age = (TextView) view.findViewById(R.id.age);
            Sex = (TextView) view.findViewById(R.id.gender);
            Email =(TextView)view.findViewById(R.id.email);
            Phone = (TextView)view.findViewById(R.id.phone);
        }
    }
}

