package com.example.xavin.loginformwithsql;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ShowData extends Activity {
    UserDataDatabase db2;
    Cursor cursor;
    RecyclerView recyclerView;
    private List<DataModel> personalData;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_data);
        personalData =  new ArrayList<>();
        db2= new UserDataDatabase(this);
        recyclerView = findViewById(R.id.recycler);
        adapter = new DataAdapter(personalData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        getData();

    }

public void getData()

    {
        cursor = db2.getData();
        if (cursor.getCount()==0)
        {
            Toast.makeText(getApplicationContext(), "No Datas Available", Toast.LENGTH_SHORT).show();
        }
            while (cursor.moveToNext()) {
            DataModel data = new DataModel("Name : "+cursor.getString(1),"Age : "+ cursor.getString(2),"Gender : "+ cursor.getString(3),"Email :"+ cursor.getString(4),"Phone No :"+ cursor.getString(5));
            personalData.add(data);
            }
    }

}
