package com.example.xavin.loginformwithsql;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button login,signup;
    EditText uname,password;
    UserInfoDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db= new UserInfoDatabase(this.getApplicationContext());

        login = findViewById(R.id.login);
        signup = findViewById(R.id.signUp);
        uname = findViewById(R.id.Username);
        password = findViewById(R.id.Password);

        if(TextUtils.isEmpty(uname.getText().toString()))
        {
            uname.setError("Name Required");
        }
        if(TextUtils.isEmpty(password.getText().toString()))
        {
            password.setError("Password Required");
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boolean validUser = db.checkLogin(uname.getText().toString(), password.getText().toString());

                if(validUser == true) {
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid login", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SignUp.class);
                startActivity(intent);
            }
        });
    }
}
