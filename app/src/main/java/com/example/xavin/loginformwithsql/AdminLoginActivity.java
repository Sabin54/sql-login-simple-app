package com.example.xavin.loginformwithsql;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLoginActivity extends Activity {

    Button login;
    EditText uname,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_login);

        uname = findViewById(R.id.Username);
        password = findViewById(R.id.Password);
        login = findViewById(R.id.login);
        if(TextUtils.isEmpty(uname.getText().toString()))
        {
            uname.setError("Name Required");
        }
        if(TextUtils.isEmpty(password.getText().toString()))
        {
            password.setError("Password Required");
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uname.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
                    Intent intent = new Intent(AdminLoginActivity.this,Admin.class);
                    startActivity(intent);
                }else{
                   Toast.makeText(getApplicationContext(),"Wrong Credentials",Toast.LENGTH_SHORT);
                }
            }
        });
    }
}
