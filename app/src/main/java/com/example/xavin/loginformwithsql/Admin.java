package com.example.xavin.loginformwithsql;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Admin extends Activity {
    UserInfoDatabase db;
    Cursor cursor;
    RecyclerView recyclerView;
    private List<AdminDataModel> AdminData;
    private AdminDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_data);
        AdminData = new ArrayList<>();
        db= new UserInfoDatabase(this);
        recyclerView = findViewById(R.id.recycler);
        adapter = new AdminDataAdapter(AdminData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        getData();

    }

    public void getData()

    {
        cursor = db.getData();
        if (cursor.getCount()==0)
        {
            Toast.makeText(getApplicationContext(), "No Datas Available", Toast.LENGTH_SHORT).show();
        }
        while (cursor.moveToNext()) {
            AdminDataModel data = new AdminDataModel("Username : "+cursor.getString(1),"Password : "+ cursor.getString(2));
            AdminData.add(data);
        }
    }

}
