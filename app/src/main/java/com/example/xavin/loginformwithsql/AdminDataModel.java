package com.example.xavin.loginformwithsql;

public class AdminDataModel {
    private String username;
    private String password;


    public AdminDataModel(String uname, String password) {
        this.username = uname;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}