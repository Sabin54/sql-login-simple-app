package com.example.xavin.loginformwithsql;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUp extends Activity {
    Button signup;
    EditText name,password;
    UserInfoDatabase db;
    private static int SPLASH_TIME_OUT = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_form);

        db=new UserInfoDatabase(this.getApplicationContext());
        name= findViewById(R.id.name);
        password=findViewById(R.id.pw);
        signup = findViewById(R.id.signup);
        if(TextUtils.isEmpty(name.getText().toString()))
        {
            name.setError("Name Required");
        }
        if(TextUtils.isEmpty(password.getText().toString()))
        {
            password.setError("Password Required");
        }
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.insertData(name.getText().toString(),password.getText().toString());
                Toast.makeText(getApplicationContext(),"Sign Up Successful", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(SignUp.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, SPLASH_TIME_OUT);
            }
        });
    }
}
