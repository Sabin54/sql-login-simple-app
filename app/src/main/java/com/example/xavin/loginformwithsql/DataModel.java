package com.example.xavin.loginformwithsql;

public class DataModel {
    private String username;
    private String age;
    private String sex;
    private String email;
    private String phone;

    public DataModel(String uname, String age, String sex, String email,String phone) {
        this.username = uname;
        this.age = age;
        this.sex = sex;
        this.email = email;
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
