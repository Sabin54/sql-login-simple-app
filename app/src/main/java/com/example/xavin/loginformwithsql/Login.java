package com.example.xavin.loginformwithsql;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
    EditText name,age,sex,phone,email;
    Button add,view;
    UserDataDatabase db1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_personal_data);

        db1= new UserDataDatabase(this.getApplicationContext());

        name= findViewById(R.id.Username1);
        age = findViewById(R.id.age);
        sex = findViewById(R.id.sex);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        add = findViewById(R.id.add);
        view = findViewById(R.id.view);

        if(TextUtils.isEmpty(name.getText().toString()))
        {
            name.setError("Name Required");
        }
        if(TextUtils.isEmpty(age.getText().toString()))
        {
            age.setError("Age Required");
        }
        if(TextUtils.isEmpty(sex.getText().toString()))
        {
            sex.setError("Gender Required");
        }
        if(TextUtils.isEmpty(phone.getText().toString()))
        {
            phone.setError("Number Required");
        }
        if(TextUtils.isEmpty(email.getText().toString()))
        {
            email.setError("Email Required");
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db1.insertData(name.getText().toString(), age.getText().toString(),sex.getText().toString(),email.getText().toString(),phone.getText().toString());
                Toast.makeText(getApplicationContext(),"Data Added",Toast.LENGTH_SHORT).show();
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,ShowData.class);
                startActivity(intent);
            }
        });
    }
}
