package com.example.xavin.loginformwithsql;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdminDataAdapter extends RecyclerView.Adapter<AdminDataAdapter.BookViewHolder> {
    private List<AdminDataModel> AdminData;

    public AdminDataAdapter(List<AdminDataModel> personalData) {
        this.AdminData = personalData;
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.admin_data, parent, false);

        return new BookViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdminDataAdapter.BookViewHolder holder, int position) {
        holder.Username.setText(AdminData.get(position).getUsername());
        holder.Password.setText(AdminData.get(position).getPassword());


    }

    @Override
    public int getItemCount() {
        return AdminData.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        public TextView Username;
        public TextView Password;


        public BookViewHolder(View view) {
            super(view);
            Username = (TextView) view.findViewById(R.id.Username2);
            Password = (TextView) view.findViewById(R.id.Password2);

        }
    }
}
