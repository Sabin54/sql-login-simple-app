package com.example.xavin.loginformwithsql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserInfoDatabase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "UserInfo.db";
    public static final String TABLE_NAME = "User";
    public static String col_1 = "ID";
    public static String col_2 = "USERNAME";
    public static String col_3 = "PASSWORD";


    public UserInfoDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(" + col_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + col_2 + " STRING, " + col_3 + " STRING)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String UserName, String Password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(col_2, UserName);
        value.put(col_3, Password);
        long result = db.insert(TABLE_NAME, null, value);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkLogin(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM User WHERE USERNAME = '"+username + "' AND PASSWORD = '" + password+"'", null);

        if(c.getCount() <= 0) {
            c.close();
            db.close();
            return false;
        } else {
            c.close();
            db.close();
            return true;
        }
    }
    public Cursor getData()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }
}
